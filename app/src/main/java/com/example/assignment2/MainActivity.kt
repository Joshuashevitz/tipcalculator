package com.example.assignment2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        calculate.setOnClickListener { handleClick() }
    }
    private fun handleClick() {
        try {
            val enteredText = you_owe.editableText.toString()

            val calculateTip = when (radioGroup.checkedRadioButtonId) {
                R.id.ten -> ConvertAmount.Ten
                R.id.twenty -> ConvertAmount.Twenty
                R.id.thirty -> ConvertAmount.Thirty
                else -> throw Exception()
            }
            val enteredValue = enteredText.toDouble()

            val result = calculateTipAmount(enteredValue, calculateTip)

            result_view.text = result.toString()
        } catch (e: Exception) {
            result_view.text = resources.getString(R.string.error)
        }
    }
}