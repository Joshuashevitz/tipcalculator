package com.example.assignment2

enum class ConvertAmount{Ten,Twenty,Thirty}

    fun calculateTipAmount(input: Double, convertAmount: ConvertAmount): Double {
       return when(convertAmount){
            ConvertAmount.Ten -> input + (input * .10)
            ConvertAmount.Twenty -> input + (input * .20)
            ConvertAmount.Thirty -> input + (input * .30)

        }

    }